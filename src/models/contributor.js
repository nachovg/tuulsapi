const mysql = require("mysql");
dbCodes = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "101237",
  database: "tools"
});

let contributorModel = {};

//recibe un codigo y suma el numero de usos de codigos de la base de datos tools
// recibe como parametro el codigo que debe buscar 
contributorModel.sumarCodigo = (code, callback) => {
  dbCodes.query(`UPDATE userdata SET codeUse = codeUse + 1 WHERE code = '${code}'`, (err, result) => {
    if (err) {
      callback(err);
    } else {
      callback(null, result);
    }
  });
}// fin de disminuir codigo

contributorModel.deleteUsertGroup = (id, callback) => {
  if (dbCodes) {
    dbCodes.query(`DELETE FROM groups WHERE id = '${id}'`, (err, res) => {
      if (err) {
        throw err;
      } else {
        callback(null, res);
      }
    });
  }
};

/*
* Contributors
*
*/

contributorModel.insertContributor = (contributor, callback) => {
  if (dbCodes) {
    dbCodes.query("INSERT INTO userdata SET ?", contributor, (err, res) => {
      if (err) {
        callback(err);
      } else {
        callback(null, res);
      }
    });
  }
};


contributorModel.checkIfContributorExist = (email, callback) => {
  if (dbCodes) {
    dbCodes.query(
      `SELECT email FROM userdata WHERE email='${email}'`,
      (err, res) => {
        if (err) {
          callback(err);
        } else {
          callback(null, res);
        }
      }
    );
  }
};


//dbcodes
contributorModel.deleteContributor = (email, callback) => {
  if (dbCodes) {
    dbCodes.query(`DELETE FROM userdata WHERE email = '${email}'`, (err, res) => {
      if (err) {
        throw err;
      } else {
        callback(null, res);
      }
    });
  }
};

contributorModel.deletegroups = (id, callback) => {
  if (dbCodes) {
    dbCodes.query(`DELETE FROM groups WHERE id = '${id}'`, (err, res) => {
      if (err) {
        throw err;
      } else {
        callback(null, res);
      }
    });
  }
};


contributorModel.checkIfUserHasCode = (email, callback) => {
  if (dbCodes) {
    return dbCodes.query(
      `SELECT code FROM userdata WHERE email = '${email}'`,
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          callback(null, rows);
        }
      }
    );
  }
};

contributorModel.checkUserCodeUse = (email, callback) => {
  if (dbCodes) {
    return dbCodes.query(
      "SELECT codeUse, maxUse FROM userdata WHERE email='" + email + "'",
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          callback(null, rows);
        }
      }
    );
  }
};

contributorModel.checkCodeUse = (code, callback) => {
  if (dbCodes) {
    return dbCodes.query(
      `SELECT codeUse, maxUse FROM userdata WHERE code = '${code}'`,
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          callback(null, rows);
        }
      }
    );
  }
};


contributorModel.resetCodeUse = (email, maxUse, code, callback) => {
  if (dbCodes) {
    return dbCodes.query(
      `UPDATE userdata SET codeUse = '${0}', maxUse = '${maxUse}', code = '${code}' WHERE email = '${email}'`,
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          callback(null, rows);
        }
      }
    );
  }
};


contributorModel.addCodeGenerated = (email, code, maxUse, callback) => {
  if (dbCodes) {
    return dbCodes.query(
      `UPDATE userdata SET code = '${code}', maxUse = ${maxUse} WHERE email = '${email}'`,
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          callback(null, rows);
        }
      }
    );
  }
};

contributorModel.checkIfCodeExist = (code, callback) => {
  if (dbCodes) {
    return dbCodes.query(
      `SELECT email FROM userdata WHERE code = '${code}'`,
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          callback(null, rows);
        }
      }
    );
  }
};

contributorModel.increaseCodeUse = (code, callback) => {
  if (dbCodes) {
    return dbCodes.query(
      `UPDATE userdata SET codeUse = codeUse + 1 WHERE code = '${code}'`,
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          callback(null, rows);
        }
      }
    );
  }
};

contributorModel.increaseMaxUse = (code, maxUse, callback) => {
  if (dbCodes) {
    return dbCodes.query(
      `UPDATE userdata SET maxUse = '${maxUse}' WHERE code = '${code}'`,
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          callback(null, rows);
        }
      }
    );
  }
};

// obtener todos los registros de userData
contributorModel.getContributors = callback => {
  if (dbCodes) {
    dbCodes.query("SELECT * FROM userdata ORDER BY email", (err, rows) => {
      if (err) {
        throw err;
      } else {
        callback(null, rows);
      }
    });
  }
};

// obtener los grupos de un contributor
contributorModel.getContributor = (email, callback) => {
  if (dbCodes) {
    dbCodes.query(`SELECT * , userdata.id as idcontributor FROM userdata LEFT JOIN groups ON groups.fk_contributor = userdata.id WHERE userdata.email='${email}'`, (err, rows) => {
      // dbCodes.query(`SELECT * FROM userData WHERE email = '${email}'` , (err, rows) => {
      if (err) {
        throw err;
      } else {
        callback(null, rows);
      }
    });
  }
};

// crear un nuevo grupo con contributor
contributorModel.addGroup = (userData, callback) => {
  if (dbCodes) {
    dbCodes.query("INSERT INTO groups (`fk_contributor`,`nombre`) VALUES (?,?)", [userData.fk_contributor, userData.nombre], (err, result) => {
      if (err) {
        callback(err);
      } else {
        callback(null, result);
      }
    });
  }
};// fin de addgroup




// selecciona todos los grupos en el que esta un usuario
contributorModel.getGroups = (fk_contributor, callback) => {
  if (dbCodes) {
    dbCodes.query(`SELECT * FROM groups WHERE fk_contributor ='${fk_contributor}'`, (err, rows) => {
      if (err) {
        throw err;
      } else {
        callback(null, rows);
      }
    });
  }
};

contributorModel.getuserGroups = callback => {
  if (dbCodes) {
    dbCodes.query("SELECT * FROM usergroup WHERE ", (err, rows) => {
      if (err) {
        throw err;
      } else {
        callback(null, rows);
      }
    });
  }
};

contributorModel.updatenamegroup = (data, callback) => {
  if (dbCodes) {
    dbCodes.query(`UPDATE groups SET nombre = '${data.nombre}' WHERE id = '${data.id}' AND fk_contributor='${data.fk_contributor}'`, (err, result) => {
      if (err) {
        callback(err);
      } else {
        callback(null, {
          insertId: result.insertId, result
        });
      }
    });
  }// fin del if 
}// fin del updatenamegroup

module.exports = contributorModel;
