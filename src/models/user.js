const mysql = require("mysql");

oldConnection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "101237",
  database: "db_redisenada_imprento"
});

connection = mysql.createPool({
  connectionLimit: 100,
  host: 'localhost',
  user: 'root',
  password: '101237',
  database: "db_impronta_tuuls",
  acquireTimeout: "100000"
});


let userModel = {};

userModel.openConexion = () => {
  // connection = mysql.createConnection({
  //   host: "localhost",
  //   user: "admin",
  //   password: "admin",
  //   database: "db_impronta_tuuls"
  // });
  // console.log("OC");
  return true;
};

userModel.closeConexion = () => {
  // connection.destroy();
  console.log("CC");
};

userModel.getUsers = callback => {
  if (connection) {
    connection.query(
      'SELECT * from user_information as u INNER JOIN animals_results as a INNER JOIN user_details as ud on u.id = a.fk_iduser AND u.id = ud.fk_iduser ORDER BY u.id',
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          callback(null, rows);
        }
      }
    );
  }
};


userModel.getUsersWithGroups = (id, i, callback) => {
  if (connection) {
    connection.query(
      'SELECT * from usergroup WHERE fk_iduser="' + id + '"',
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          console.log(rows);
          callback(null, rows, i);
        }
      }
    );
  }
}// fin de getUsersWithGropus

userModel.getUsersResults = callback => {
  if (connection) {
    connection.query(
      'SELECT * from user_details as ud INNER JOIN response_value as rv INNER JOIN  user_information as ui on ud.fk_iduser = rv.fk_iduser AND ui.id = ud.fk_iduser and ui.deleted_at IS NULL and rv.versionactiva IS TRUE',
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          callback(null, rows);
        }
      }
    );
  }
};
// connection.query(`SELECT user_details.* FROM user_details WHERE EXISTS (SELECT * from response_value where fk_email= user_details.fk_email) ORDER BY user_details.fk_email`, (err, rows) => {



userModel.getRelatedUsers = (code, callback) => {
  if (connection) {
    connection.query(
      // "SELECT * FROM userData WHERE code = '" + code + "'",
      "SELECT user_information.*, animals_results.* FROM animals_results LEFT OUTER JOIN user_information ON animals_results.fk_iduser = user_information.id WHERE user_information.code = '" + code + "' and user_information.deleted_at IS NULL and animals_results.versionactiva IS TRUE ",
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          callback(null, rows);
        }
      }
    );
  }
};

userModel.getSpecificUsers = (iduser, callback) => {
  if (connection) {
    connection.query(
      // "SELECT * FROM user_information as u INNER JOIN user_details as ud on ud.fk_iduser= u.id WHERE u.id = '" + iduser + "'",
      "SELECT * FROM user_information as u LEFT OUTER JOIN user_details as ud on ud.fk_iduser= u.id WHERE u.id = '" + iduser + "'",
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          callback(null, rows);
        }
      }
    );
  }
};


userModel.getVersiosUser = (userid, callback) => {
  if (connection) {
    connection.query(
      "SELECT * FROM userversion WHERE fk_iduser = '" + userid + "'",
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          if (rows.length > 0) {
            callback(null, { 'fk_iduser': rows[rows.length - 1].fk_iduser, 'version': (rows[rows.length - 1].numero_prueba) + 1, 'id': rows[rows.length - 1].id, 'existe': true });
          } else {
            callback(null, { 'version': 1, 'id': 1, 'existe': false });
          }
        }
      }
    );
  }
};


userModel.insertVersion = (userData, suma, callback) => {
  if (connection) {
    connection.query("INSERT INTO userversion (`fk_iduser`,`numero_prueba`) VALUES (?,?)", [userData, suma], (err, result) => {
      if (err) {
        callback(err);
      } else {
        callback(null, result);
      }
    });
  }// if

};

userModel.insertUser = (userData, callback) => {
  if (connection) {
    connection.query("INSERT INTO user_information (`email`,`username`,`code`) VALUES (?,?,?)", [userData.email, userData.username, userData.code], (err, result) => {
      if (err) {
        callback(err);
      } else {
        callback(null, result);

      }
    });
  }
};


userModel.insertIndividualResults = (userData, fin, callback) => {
  if (connection) {
    connection.query("INSERT INTO response_value SET ?", userData, (err, result) => {
      callback(null, result, (fin + 1));
    })
  }
}


userModel.insertUserDetails = (userDataDetails, callback) => {
  if (connection) {
    connection.query("INSERT INTO user_details (`fk_iduser`, `birthday`, `country`, `education`, `employment`, `gender`, `region`, `status`, `representacion`, `utilidad`, `recomendacion`) VALUES (?,?,?,?,?,?,?,?,?,?,?)",
      [
        userDataDetails.fk_iduser,
        userDataDetails.birthday,
        userDataDetails.country,
        userDataDetails.education,
        userDataDetails.employment,
        userDataDetails.gender,
        userDataDetails.region,
        userDataDetails.status,
        userDataDetails.representacion,
        userDataDetails.utilidad,
        userDataDetails.recomendacion,
        userDataDetails.currentDay
      ], (err, result) => {
        if (err) {
          callback(err);
        } else {
          callback(null, true);
        }// fin del else
      });// fin del query insert
  }// fin del if
}// fin del metodo insertUserDetails 

userModel.checkEnabledVersion = (iduser, callback) => {
  if (connection) {
    connection.query(
      `SELECT * from user_information WHERE id = '${iduser}' AND enabledversion IS TRUE`,
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          callback(null, rows);
        }
      }
    );
  }
};

userModel.enabled_version = (iduser, valor, callback) => {
  console.log(iduser + ' - ' + valor);
  if (connection) {
    connection.query(`UPDATE user_information SET enabledversion = '${valor}'  WHERE id = '${iduser}'`, (err, result) => {
      if (err) {
        console.log(err);
        callback(err);
      } else {
        callback(null, result);
        console.log(result);
      }
    });
  }// fin del if 
}// fin del enabled version

userModel.updateUserDetails = (userData, callback) => {
  if (connection) {
    connection.query(`UPDATE user_details SET birthday = '${userData.birthday}' , country='${userData.country}', education='${userData.education}',employment='${userData.employment}',gender='${userData.gender}',region='${userData.region}',status='${userData.status}'  WHERE fk_iduser = '${userData.fk_iduser}'`, (err, result) => {
      if (err) {
        console.log(err);
        callback(err);
      } else {
        console.log(result);
        callback(null, result);
      }
    });
  }// fin del if 
}// fin del updatenamegroup

userModel.checkUserDetails = (fk_iduser, callback) => {
  if (connection) {
    connection.query(
      `SELECT * FROM user_details WHERE fk_iduser = '${fk_iduser}'`,
      (err, rows) => {
        if (err) {
          throw err;
        } else {
          callback(null, rows);
        }
      }
    );
  }
};//fin de checkUserDetails



userModel.insertUserAnimals = (userDataAnimals, id, callback) => {
  connection.query("INSERT INTO animals_results (`jaguar`, `dragon`, `colibri`, `cisne`, `halcon`, `serpiente`, `caballo`, `leon`, `condor`, `ballena`, `oso`, `buho`, `firstAnimalPercentage`, `secondAnimalPercentage`, `fk_iduser`,`idversion`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
    [
      userDataAnimals.Jaguar,
      userDataAnimals.Dragon,
      userDataAnimals.Colibri,
      userDataAnimals.Cisne,
      userDataAnimals.Halcon,
      userDataAnimals.Serpiente,
      userDataAnimals.Caballo,
      userDataAnimals.Leon,
      userDataAnimals.Condor,
      userDataAnimals.Ballena,
      userDataAnimals.Oso,
      userDataAnimals.Buho,
      userDataAnimals.firstAnimalPercentage,
      userDataAnimals.secondAnimalPercentage,
      userDataAnimals.fk_iduser,
      id
    ], (err, result) => {
      if (err) {
        callback(err);
      } else {
        callback(null, true);
      }// fin del else
    });// fin del query insert

}// fin del insertUserAnimals

//editado
userModel.checkIfUserExists = (email, code, callback) => {
  if (connection) {
    connection.query(
      `SELECT id,email,code,enabledversion FROM user_information WHERE email='${email}' and code='${code}' and deleted_at IS NULL`,
      (err, res) => {
        if (err) {
          callback(err);
        } else {
          callback(null, res);
        }
      }
    );
  }
};


userModel.updateQuestions = (userData, callback) => {
  if (connection) {
    connection.query(`UPDATE user_details SET representacion = '${userData.representacion}',utilidad = '${userData.utilidad}',recomendacion = '${userData.recomendacion}' WHERE fk_iduser = '${userData.iduser}'`, (err, res) => {
      if (err) {
        callback(err);
      } else {
        callback(null, res);
      }
    });
  }
}// fin de updateQuestions

//agregar un registro con grupo y usuario a db testsql
userModel.userGroup = (userData, callback) => {
  if (connection) {
    connection.query("INSERT INTO usergroup (`fk_iduser`,`fk_group`) VALUES (?,?)", [userData.fk_iduser, userData.fk_group], (err, result) => {
      if (err) {
        callback(err);
      } else {
        callback(null, result);
      }
    });
  }
};// fin de addgroup

// obtener los grupos de un contributor
userModel.getUserDetailsExisteResponse = (callback) => {
  if (connection) {
    connection.query(`SELECT user_details.* FROM user_details WHERE EXISTS (SELECT * from response_value where fk_iduser= user_details.fk_iduser) ORDER BY user_details.fk_iduser`, (err, rows) => {
      if (err) {
        throw err;
      } else {
        callback(null, rows);
      }
    });
  }
};



userModel.deleteUsertIndividual = (userData, callback) => {
  if (connection) {
    connection.query(`DELETE FROM usergroup WHERE fk_iduser = '${userData.fk_iduser}' AND fk_group='${userData.fk_group}'`, (err, res) => {
      if (err) {
        throw err;
      } else {
        callback(null, res);
      }
    });
  }
};

userModel.deleteAllUserGroups = (fk_group, callback) => {
  console.log(fk_group + 'allusergroups')
  if (connection) {
    connection.query(`DELETE FROM usergroup WHERE fk_group='${fk_group}'`, (err, res) => {
      console.log('next is res')
      console.log(res)
      if (err) {
        throw err;
      } else {
        callback(null, res);
      }
    });
  }
};


//poner id
userModel.deleteUserRegistry = (id, callback) => {
  if (connection) {
    connection.query(`UPDATE user_information SET code = '' WHERE id = '${id}'`, (err, result) => {
      if (err) {
        callback(err);
      } else {
        callback(null, {
          insertId: result.insertId, result
        });
      }
    });
  }// fin del if 
}// fin del updatenamegroup

// poner id
userModel.addUserPosition = (position, id, callback) => {
  if (connection) {
    connection.query(`UPDATE user_information SET position = '${position}' WHERE id = '${id}'`, (err, result) => {
      if (err) {
        callback(err);
      } else {
        callback(null, {
          insertId: result.insertId, result
        });
      }
    });
  }// fin del if 
}// fin del updatenamegroup

Date.prototype.format = function (fstr, utc) {
  var that = this;
  utc = utc ? 'getUTC' : 'get';
  return fstr.replace(/%[YmdHMS]/g, function (m) {
    switch (m) {
      case '%Y': return that[utc + 'FullYear']();
      case '%m': m = 1 + that[utc + 'Month'](); break;
      case '%d': m = that[utc + 'Date'](); break;
      case '%H': m = that[utc + 'Hours'](); break;
      case '%M': m = that[utc + 'Minutes'](); break;
      case '%S': m = that[utc + 'Seconds'](); break;
      default: return m.slice(1);
    }
    return ('0' + m).slice(-2);
  });
};

userModel.getFecha = () => {
  let fechaHoy = new Date();
  return fechaHoy = fechaHoy.format("%Y-%m-%d", true);
}

userModel.deleteEncuesta = (iduser, callback) => {
  if (connection) {
    fechadelete = new Date();
    console.log(fechadelete);
    console.log(fechadelete.format("%Y-%m-%d", true));
    fechadelete = fechadelete.format("%Y-%m-%d", true);
    connection.query(`UPDATE user_information SET deleted_at = '${fechadelete}' WHERE id = '${iduser}'`, (err, result) => {
      if (err) {
        callback(err);
      } else {
        callback(null, result);
      }
    });
  }// fin del if 
}// fin del metodo elimitar usuario de encuesta


//optiene el ultimo id version del usuario
userModel.disabledOldVersionResponse = (idversion, fk_iduser, callback) => {
  if (connection) {
    connection.query(`update response_value set versionactiva=0 WHERE fk_iduser='${fk_iduser}' and idversion != '${idversion}'`, (err, rows) => {
      if (err) {
        throw err;
      } else {
        callback(null, rows);
      }
    });
  }
};

//optiene el ultimo id version del usuario
userModel.disabledOldVersionAnimal = (idversion, fk_iduser, callback) => {
  if (connection) {
    connection.query(`update animals_results set versionactiva=0 WHERE fk_iduser='${fk_iduser}' and idversion != '${idversion}'`, (err, rows) => {
      if (err) {
        throw err;
      } else {
        callback(null, rows);
      }
    });
  }
};

userModel.getAllUser = (callback) => {
  if (connection) {
    connection.query(`SELECT id FROM user_information`, (err, rows) => {
      if (err) {
        throw err;
      } else {
        callback(null, rows);
      }
    });
  }
};

// userModel.getLastVersion = (id, callback) => {
//   if (connection) {
//     connection.query(`SELECT id,fk_iduser FROM userversion where fk_iduser='${id}'`, (err, rows) => {
//       if (err) {
//         throw err;
//       } else {
//         if (rows.length > 0) {
//           callback(null, rows[rows.length - 1]);
//         }
//       }
//     });
//   }
// };

// userModel.updateVersionActiva = (fk_iduser, idversion, callback) => {
//   // console.log(fk_iduser+"----"+idversion)

//   if (connection) {
//     // connection.query(`update animals_results set versionactiva=0 WHERE fk_iduser='${fk_iduser}' and idversion != '${idversion}'`, (err, rows) => {
//     connection.query(`update response_value set versionactiva=0 WHERE fk_iduser='${fk_iduser}' and idversion != '${idversion}'`, (err, rows) => {
//       if (err) {
//         throw err;
//       } else {
//         callback(null, rows);
//       }
//     });
//   }
// };

// userModel.getData = (callback) => {
//   if (connection) {
//     connection.query(`select * from user_information`, (err, rows) => {
//       let response = rows;

//       if (err) {
//         throw err;
//       } else {
//         callback(null, rows);
//       }
//     });
//   }
// };

// userModel.addData = (id, email, callback) => {
//   let newData = [];
//   let oldData = [];

//   if (connection) {
//     oldConnection.query(`select * from response_value where fk_email = '${email}' and idversion is NULL`, (err, colums) => {
//       oldData = colums;

//       if (err) {
//         console.log('oldConnection' + JSON.stringify(err));
//       }
//       else {
//         callback(null, colums, id);
//       }
//       // } else if (colums.length > 0) {

//       //   connection.query(`select * from userversion where fk_iduser = '${id}'`, (err, rows) => {
//       //     newData = rows;
//       //     if (err) {
//       //       console.log('connection' + JSON.stringify(err));
//       //     } else {
//       //       callback(null, newData, oldData);
//       //     }
//       //   });


//       //   // for (i = 0; i < colums.length; i++) {

//       //   // connection.query(`INSERT INTO usergroup (fk_iduser, fk_group) values ('${id}', '${colums[i].fk_group}')`, (err, res) => {
//       //   //   if (err) {
//       //   //     console.log(err);
//       //   //   }
//       //   // });
//       //   // }
//       // }
//       // console.log(email);
//     });
//   }
// }

// userModel.compareData = (column, id, callback) => {
//   // console.log(idversion + ' - ' + email + ' - ' + id);
//   // console.log(i)
//   // oldConnection.query(`select * from animals_results where fk_email = '${email}' and idversion = '${idversion}'`, (err, column) => {
//   //   // console.log(email + '- ' + idversion);
//   //   // console.log(column[0].jaguar);
//   //   console.log(column.length);
//   //   if (err) {
//   //     console.log(err);
//   //   } else {
//   //     if (column.length > 0) {
//   connection.query(`INSERT INTO response_value (creativo, sociable, valioso, visible, visionario, innovador, astuto, intuitivo, solidario, generoso, independiente, reservado, question, fk_iduser) values ('${column.creativo}', '${column.sociable}', '${column.valioso}', '${column.visible}', '${column.visionario}', '${column.innovador}', '${column.astuto}', '${column.intuitivo}', '${column.solidario}', '${column.generoso}', '${column.independiente}', '${column.reservado}', '${column.question}', '${id}')`, (err, res) => {
//     if (err) {
//       console.log(err);
//     } else {
//       console.log('success');
//     }
//   });
//   // }

//   // }
//   // });


// };


// userModel.insertNewData = (column, userID, newVersionID, callback) => {

//   connection.query(`INSERT INTO response_value (creativo, sociable, valioso, visible, visionario, innovador, astuto, intuitivo, solidario, generoso, independiente, reservado, question, fk_iduser, idversion) values ('${column.creativo}', '${column.sociable}', '${column.valioso}', '${column.visible}', '${column.visionario}', '${column.innovador}', '${column.astuto}', '${column.intuitivo}', '${column.solidario}', '${column.generoso}', '${column.independiente}', '${column.reservado}', '${column.question}', '${userID}', '${newVersionID}')`, (err, res) => {
//     if (err) {
//       console.log(err);
//     } else {
//       console.log('success');
//     }
//   });

// };

module.exports = userModel;

