const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
var cors = require("cors");

app.use(cors());

//Settings
app.set("port", process.env.PORT || 8001);

//Middlewares
app.use(morgan("dev"));
app.use(bodyParser.json());

//routes
require("./routes/userRoutes")(app);

app.listen(app.get("port"), () => {
  console.log("server on port 8000");
});
