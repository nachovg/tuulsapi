const User = require("../models/user");
const Contributor = require("../models/contributor");
const crypto = require("crypto");
const fs = require("fs");

module.exports = function (app) {
  app.get("/api/:iduser", (req, res) => {
    var id = req.params.iduser;
    User.openConexion();
    User.getSpecificUsers(id, (err, data) => {
      User.closeConexion();
      res.status(200).json(data);
    });
  });

  // Devuelve todo
  app.get("/api", (req, res) => {
    User.openConexion();
    User.getUsers((err, data) => {
      User.closeConexion();
      res.status(200).json(data);
    });
  });


  var datos;
  app.get("/api/related/:code", (req, res) => {
    var code = req.params.code;
    User.openConexion();
    User.getRelatedUsers(code, (err, data) => {
      this.datos = data;
      console.log(data.length);
      if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
          this.datos[i].groups = {};
          User.getUsersWithGroups(data[i].fk_iduser, i, (err, data, va) => {
            this.datos[va].groups = data;
            if (va == (this.datos.length - 1)) {
              User.closeConexion();
              res.status(200).json(this.datos);
            }
          });
        }//fin del for
      }//if
      else {
        User.closeConexion();
        res.status(405).json({ success: false, msg: "No hay registros", data: data, error: err });
      }
    });
  });// fin de /api/related/:code

  // // Devuelve todo
  app.get("/userinfoversion/:iduser", (req, res) => {
    let iduser = req.params.iduser;
    User.openConexion();
    User.getVersiosUser(iduser, (err, data) => {
      User.closeConexion();
      res.status(200).json(data);
    });
  });

  // Devuelve todos los resultados individuales de todos los usuarios
  app.post("/individualResults", (req, res) => {
    User.openConexion();
    User.changeUserStatus((err, data) => {
      User.closeConexion();
      res.status(200).json(data);
    });
  });

  // recoge todos los datos de las encuesta y guarda el resultado
  app.post("/api/userData", (req, res) => {
    //codigo para escribir en archivos planos
    let response = res;
    let individualResults = req.body.individualResults;
    let respuestas = "[";
    // fin de codigo para escribir
    for (var i = 0; i < individualResults.length; i++) {
      respuestas +=
        '{"creativo":"' + individualResults[i].Creativo + '"' +
        ',"sociable":"' + individualResults[i].Sociable + '"' +
        ',"valioso":"' + individualResults[i].Valioso + '"' +
        ',"visible":"' + individualResults[i].Visible + '"' +
        ',"visionario":"' + individualResults[i].Visionario + '"' +
        ',"innovador":"' + individualResults[i].Innovador + '"' +
        ',"astuto":"' + individualResults[i].Astuto + '"' +
        ',"intuitivo":"' + individualResults[i].Intuitivo + '"' +
        ',"solidario":"' + individualResults[i].Solidario + '"' +
        ',"generoso":"' + individualResults[i].Generoso + '"' +
        ',"independiente":"' + individualResults[i].Independiente + '"' +
        ',"reservado":"' + individualResults[i].Reservado + '"' +
        ',"question":"' + individualResults[i].question + '" },';
    }// fin de concatenacion 
    fs.appendFile(User.getFecha() + ".txt",
      '{ "username":"' + req.body.username + '"' +
      ',"code":"' + req.body.code + '"' +
      ',"email":"' + req.body.email + '"' +
      ',"birthday":"' + req.body.age + '"' +
      ',"country":"' + req.body.country + '"' +
      ',"education":"' + req.body.education + '"' +
      ',"employment":"' + req.body.employment + '"' +
      ',"gender":"' + req.body.gender + '"' +
      ',"region":"' + req.body.region + '"' +
      ',"status":"' + req.body.status + '"' +
      ',"currentDate":"' + req.body.currentDate + '"' +
      ',"Jaguar":"' + req.body.Jaguar + '"' +
      ',"Dragon":"' + req.body.Dragon + '"' +
      ',"Colibri":"' + req.body.Colibri + '"' +
      ',"Cisne":"' + req.body.Cisne + '"' +
      ',"Halcon":"' + req.body.Halcon + '"' +
      ',"Serpiente":"' + req.body.Serpiente + '"' +
      ',"Caballo":"' + req.body.Caballo + '"' +
      ',"Leon":"' + req.body.Leon + '"' +
      ',"Condor":"' + req.body.Condor + '"' +
      ',"Ballena":"' + req.body.Ballena + '"' +
      ',"Oso":"' + req.body.Oso + '"' +
      ',"Buho":"' + req.body.Buho + '"' +
      ',"secondAnimalPercentage":"' + req.body.secondAnimalPercentage + '"' +
      ',"firstAnimalPercentage":"' + req.body.firstAnimalPercentage + '"' +
      ',"respuesta":' + respuestas + ']}'
      , (err) => { });



    const userData = {
      username: req.body.username,
      code: req.body.code,
      email: req.body.email,
    };
    const userDataDetails = {
      birthday: req.body.age,
      country: req.body.country,
      education: req.body.education,
      employment: req.body.employment,
      gender: req.body.gender,
      region: req.body.region,
      status: req.body.status,
      currentDate: req.body.currentDate
    };
    const userDataAnimals = {
      Jaguar: req.body.Jaguar,
      Dragon: req.body.Dragon,
      Colibri: req.body.Colibri,
      Cisne: req.body.Cisne,
      Halcon: req.body.Halcon,
      Serpiente: req.body.Serpiente,
      Caballo: req.body.Caballo,
      Leon: req.body.Leon,
      Condor: req.body.Condor,
      Ballena: req.body.Ballena,
      Oso: req.body.Oso,
      Buho: req.body.Buho,
      secondAnimalPercentage: req.body.secondAnimalPercentage,
      firstAnimalPercentage: req.body.firstAnimalPercentage,
    };

    let idVersion;
    let userid;
    User.openConexion();
    User.checkIfUserExists(userData.email, userData.code, (err, data) => {
      if (data.length > 0) {
        userid = data[0].id;
        User.checkEnabledVersion(userid, (err, datoscheck) => {
          if (datoscheck.length > 0) {
            User.enabled_version(userid, 0, (err, data) => { });
            User.getVersiosUser(userid, (err, resultado) => {
              // si el usuario ya tiene una version, se trae la cantidad de la version y se crea uno nuevo con la nueva cantidad
              if (resultado.existe) {
                User.insertVersion(userid, resultado.version, (err, resultados) => {
                  idVersion = resultados.insertId;
                  userDataDetails.fk_iduser = userid;


                  User.checkUserDetails(userid, (err, data) => {
                    if (data.length > 0) {
                      User.openConexion();
                      User.updateUserDetails(userDataDetails, (err, dati) => { console.log("actualiazcion details" + dati); });
                    } else {
                      User.insertUserDetails(userDataDetails, (err, data) => {
                      });
                      // console.log(data[0]);
                    }// fin del else
                  });
                  userDataAnimals.fk_iduser = userid;
                  User.insertUserAnimals(userDataAnimals, idVersion, (err, data) => {
                    // deshabilitar version anteriores
                    User.disabledOldVersionAnimal(idVersion, userid, (e, d) => { });
                  });
                  // se registran todos los datos, y se devuelve un success y se disminuye el codigo hasta que todo tenga success
                  for (i = 0; i < individualResults.length; i++) {
                    individualResults[i].fk_iduser = userid;
                    individualResults[i].idversion = idVersion;
                    User.insertIndividualResults(individualResults[i], i, (err, data, fin) => {
                      if (fin == individualResults.length) {
                        Contributor.sumarCodigo(userData.code, (err, data) => { });
                        // deshabilitar versiones anteriores
                        User.disabledOldVersionResponse(idVersion, userid, (e, d) => { });
                        User.closeConexion();
                        res.status(200).json({ id: userid, success: true, msg: "El usuario se actualizo exitosamente", data });
                        res.end(data);
                      }
                      if (err) {
                        res.status(405).json({ success: false, msg: "Error al intentantar agregar al usuario", data: data, error: err });
                      }
                    })
                  } // fin del for
                  // fin de inser version si existe uno
                });
              }
              // sin el usuario no tiene version se crea uno nuevo y empiza desde 1
              else {
                Console.log("version no existe");
                User.insertVersion(userid, 1, (err, datas) => {
                  idVersion = datas.insertId;
                  userDataDetails.fk_iduser = userid;
                  User.checkUserDetails(userid, (err, data) => {
                    if (data > 0) {
                      User.updateUserDetails(userDataDetails, (err, data) => { });
                    } else {
                      User.insertUserDetails(userDataDetails, (err, data) => { });
                    }// fin del else
                  });

                  userDataAnimals.fk_iduser = userid;
                  User.insertUserAnimals(userDataAnimals, idVersion, (err, data) => { });
                  // se registran todos los datos, y se devuelve un success y se disminuye el codigo hasta que todo tenga success
                  for (i = 0; i < individualResults.length; i++) {
                    individualResults[i].fk_iduser = userid;
                    individualResults[i].idversion = idVersion;
                    User.insertIndividualResults(individualResults[i], i, (err, data, fin) => {
                      console.log(fin);
                      if (fin == individualResults.length) {
                        Contributor.sumarCodigo(userData.code, (err, data) => { });
                        User.closeConexion();
                        res.status(200).json({ id: userid, success: true, msg: "El usuario se agregó exitosamente", data });
                        res.end(data);
                      }
                      if (err) {
                        res.status(405).json({ success: false, msg: "Error al intentantar agregar al usuario", data: data, error: err });
                      }
                    })
                  }
                  // fin de inser version si no existe              
                });
              }
            });//getVersiosUser
          } else {
            //User.closeConexion();
            res.status(404).json({ id: userid, success: true, msg: "El usuario no habilidato nuevo test", data });
            res.end(data);
          }
        });// fin de checkenableversion
      }// if de si existe 
      // si no existe o si se va hacer el primer registro
      if (data.length == 0) {
        User.insertUser(userData, (err, data) => {
          userid = data.insertId;
          if (err == null) {
            User.insertVersion(userid, 1, (err, resultados) => {
              idVersion = resultados.insertId;
              userDataDetails.fk_iduser = userid;
              User.insertUserDetails(userDataDetails, (err, data) => {

              });
              userDataAnimals.fk_iduser = userid;
              User.insertUserAnimals(userDataAnimals, idVersion, (err, data) => {

              });
              for (i = 0; i < individualResults.length; i++) {
                individualResults[i].fk_iduser = userid;
                individualResults[i].idversion = idVersion;
                User.insertIndividualResults(individualResults[i], i, (err, data, fin) => {
                  //console.log(fin);
                  if (fin == individualResults.length) {
                    Contributor.sumarCodigo(userData.code, (err, data) => { });
                    res.status(200).json({ success: true, msg: "El usuario se agregó exitosamente", data });
                    res.end(data);
                  }
                  if (err) {
                    res.status(405).json({ success: false, msg: "Error al intentantar agregar al usuario", data: data, error: err });
                  }
                })
              }

            });// insertar la nueva version
          }// si el insert fue exitoso
        });
      };

    }); //checkIfUserExists

  });


  //habilita crear una nueva version de test
  app.post("/api/allowToTest", (req, res) => {
    let iduser = req.body.iduser;
    let enable = req.body.enable;
    User.openConexion();
    User.enabled_version(iduser, enable, (err, data) => {
      console.log(data)

      User.closeConexion();
      res.status(200).json({
        success: true,
        msg: "Habilidar version",
        data
      });
      res.end(data);
    });
  }// fin de peticion 
  )
  // Actualiza la encuesta una ves se han agregado los valores de la escuenta
  //requiere como identificador el iduser
  app.post("/api/update", (req, res) => {
    const userData = {
      recomendacion: req.body.recomendacion,
      utilidad: req.body.utilidad,
      representacion: req.body.representacion,
      iduser: req.body.iduser
    }
    User.openConexion();
    User.updateQuestions(userData, (err, data) => {
      if (data) {
        User.closeConexion();
        res.status(200).json({
          success: true,
          msg: "El usuario se actualizó exitosamente",
          data
        });
        res.end(data);
      } else {
        User.closeConexion();
        res.status(405).json({
          success: false,
          msg: "Error al intentantar actulizar las preguntas",
          data: data,
          error: err
        });
        res.end(data);
      }
    });

  });
  /*
  * Contributors API
  *
  */

  // Agrega nuevo contribuidor
  app.post("/new/contributor", (req, res) => {
    const codeGenerated = crypto.randomBytes(3).toString("hex");

    const contributorData = {
      email: req.body.email,
      code: codeGenerated,
      maxUse: req.body.maxUse,
      codeUse: 0,
      companyName: req.body.companyName
    };

    Contributor.checkIfContributorExist(contributorData.email, (err, data) => {
      if (data.length > 0) {
        res.status(200).json({
          success: false,
          msg: "Este email ya está registrado"
        });
      } else {
        Contributor.insertContributor(contributorData, (err, data) => {
          if (data) {
            res.status(200).json({
              success: true,
              msg: "Contribuidor agregado exitósamente"
            });
          } else {
            res.status(405).json({
              success: false,
              msg: "Error al intentantar agregar al usuario",
              data: data,
              error: err
            });
          }
        });
      }
    });
  });

  // elimina contribuidor
  app.delete("/contributor/:email", (req, res) => {
    email = req.params.email;

    Contributor.deleteContributor(email, (err, data) => {
      if (data.affectedRows > 0) {
        res.status(200).json({
          success: true,
          msg: "Contribuidor eliminado"
        });
      } else {
        res.status(405).json({
          success: false,
          msg: "No se encontró el usuario que se desea eliminar"
        });
      }
    });
  });

  // elimina grupo de las dos db// elimina los grupos
  app.delete("/api/deletegroup", (req, res) => {
    id = req.body.id;
    Contributor.deletegroups(id, (err, data) => {
      if (data.affectedRows > 0) {
        User.openConexion();
        User.deleteAllUserGroups(id, (err, data) => {
          if (err == null) {
            User.closeConexion();
            res.status(200).json({
              success: true,
              msg: "Contribuidor eliminado"
            });
          } else {
            User.closeConexion();
            res.status(405).json({
              success: false,
              msg: "No se encontró el usuario que se desea eliminar"
            });
          }
        });

      } else {
        res.status(405).json({
          success: false,
          msg: "No se encontró el usuario que se desea eliminar"
        });
      }
    });
  });

  // trae un array de objetos con todos los contribuidores
  app.get("/getContributors", (req, res) => {
    Contributor.getContributors((err, data) => {
      res.status(200).json(data);
    });
  });

  // trae un array de objetos con un contributor con los grupos que tiene
  app.get("/getContributor/:email", (req, res) => {
    email = req.params.email;
    Contributor.getContributor(email, (err, data) => {
      res.status(200).json(data);
    });
  });

  // agrega código a un usuario
  app.patch("/contributors/:email/code/:maxUse", (req, res) => {
    let email = req.params.email;
    let maxUse = req.params.maxUse;
    Contributor.checkIfUserHasCode(email, (err, resp) => {
      if (resp[0].code != null) {
        Contributor.checkUserCodeUse(email, (err, response) => {
          if (response[0].codeUse == response[0].maxUse) {
            const codeGenerated = crypto.randomBytes(3).toString("hex");
            Contributor.resetCodeUse(email, maxUse, codeGenerated, (err, response) => {
              res.status(200).json({
                status: 200,
                msg: "Código creado exitósamente",
                code: codeGenerated
              });
            });
          } else {
            res.status(200).json({
              status: 403,
              msg:
                "El usuario ya tiene un código asignado y no ha alcanzado su límite"
            });
          }
        });
      } else {
        const codeGenerated = crypto.randomBytes(3).toString("hex");
        Contributor.addCodeGenerated(email, codeGenerated, maxUse, (err, response) => {
          res.status(200).json({
            status: 200,
            msg: "Código creado exitósamente",
            code: codeGenerated
          });
        });
      }
    });
  });

  // revisa si un código ya existe
  app.post("/api/check/code", (req, res) => {
    let code = req.body.code;
    let email = req.body.email;
    let tempResponseUser = {};
    Contributor.checkIfCodeExist(code, (err, response) => {
      console.log(response);
      if (response.length == 0) {
        res.status(200).json({
          status: 204,
          msg: "Código no existe"
        });
      } else {
        User.checkIfUserExists(email, code, (err, response) => {
          if (response.length == 0) {
            res.status(200).json({
              status: 200,
              msg: "Usuario puede realizar la prueba"
            })
          } else {
            tempResponseUser = response;
            Contributor.checkCodeUse(code, (err, response) => {
              if (response[0].codeUse >= response[0].maxUse) {
                res.status(405).json({
                  status: 405,
                  msg: "El uso del código ha alcanzado su límite"
                });
              } else {
                console.log(tempResponseUser[0].enabledversion);
                if (tempResponseUser[0].enabledversion == '1') {
                  res.status(200).json({
                    status: 200,
                    msg: "Usuario puede realizar la prueba"
                  })
                } else {
                  res.status(405).json({
                    status: 405,
                    msg: "Usuario no tiene permiso de realizar la prueba, contacte a su administrador"
                  })
                }
              }
            })


          }
        });


      }
    });
  });

  // aumenta el uso del código (maxUse)
  app.patch("/api/used/code/:code", (req, res) => {
    let code = req.params.code;
    Contributor.checkIfCodeExist(code, (err, response) => {
      if (response.length == 0) {
        res.status(200).json({
          status: 204,
          msg: "Código no existe"
        });
      } else {
        Contributor.checkCodeUse(code, (err, response) => {
          if (response[0].codeUse >= response[0].maxUse) {
            res.status(405).json({
              status: 405,
              msg: "El uso del código ha alcanzado su límite"
            });
          } else {
            Contributor.increaseCodeUse(code, (err, response) => {
              res.status(200).json({
                status: 200,
                msg: "Uso de código +1"
              });
            });
          }
        });
      }
    });
  });

  // aumenta el máximo
  app.patch("/api/:code/code/:maxValue", (req, res) => {
    console.log(req)

    let code = req.params.code;
    let maxValue = req.params.maxValue;


    Contributor.checkIfCodeExist(code, (err, response) => {
      if (response.length == 0) {
        res.status(200).json({
          status: 204,
          msg: "Código no existe"
        });
      } else {
        Contributor.increaseMaxUse(code, maxValue, (err, response) => {
          res.status(200).json({
            success: true,
            status: 200,
            msg: "Se ha aumentado el máximo de usos del código"
          });
        });
      }
    });
  });

  // revisa si un código ya existe// revisar
  app.get("/api/check/userID", (req, res) => {
    let userId = crypto.randomBytes(4).toString("hex")
    Contributor.userIdExists(userId, (err, response) => {
      if (response.length == 0) {
        res.status(200).json({
          status: 204,
          msg: "Código no existe",
          id: userId
        });
      } else {

      }
    });
  });// sin de check

  //  agrega un grupo, un contributor agrega nombre y id del contributor
  app.post("/api/addgroup", (req, res) => {
    console.log(req.body.fk_contributor);
    const contributorData = {
      fk_contributor: req.body.fk_contributor,
      nombre: req.body.nombre
    };
    Contributor.addGroup(contributorData, (err, data) => {
      res.status(200).json({
        status: 200,
        msg: "Agregado exitoso"
      });
    });
  });

  // agrega a usergroup en db impronta
  app.post("/api/userGroup", (req, res) => {
    const contributorData = {
      fk_group: req.body.fk_group,
      fk_iduser: req.body.fk_iduser
    };
    User.openConexion();
    User.userGroup(contributorData, (err, data) => {
      User.closeConexion();
      res.status(200).json(data);
    });
  });

  app.delete("/deleteusergroup", (req, res) => {
    let id = req.body.id;
    Contributor.deleteUsertGroup(id, (err, data) => {
      if (data.affectedRows > 0) {
        User.openConexion();
        User.deleteAllUserGroups(id, (err, datos) => {
          if (datos.affectedRows > 0) {
            User.closeConexion();
            res.status(200).json({
              success: true,
              msg: "Grupo eliminado eliminado",
              data: data
            });
          }//if
          else {
            User.closeConexion();
            res.status(405).json({
              success: false,
              msg: "No se encontró el grupo que se desea eliminar"
            });
          }//else
        });

      } else {
        res.status(405).json({
          success: false,
          msg: "No se encontró el grupo que se desea eliminar"
        });
      }
    });
  });

  // eliminar a un usuario de un grupo con el id del usuario y el codigo del grupo

  app.delete("/api/deleteSurvey", (req, res) => {
    let fk_iduser = req.body.fk_iduser;
    User.openConexion();
    User.deleteEncuesta(fk_iduser, (err, data) => {
      if (data.affectedRows > 0) {
        User.closeConexion();
        res.status(200).json({
          success: true,
          msg: "Usuario eliminado"
        });
      }
    })
  });

  app.delete("/deleteuserIndividual", (req, res) => {
    const userData = {
      fk_group: req.body.fk_group,
      fk_iduser: req.body.fk_iduser
    };
    User.openConexion();
    User.deleteUsertIndividual(userData, (err, data) => {
      if (data.affectedRows > 0) {
        User.closeConexion();
        res.status(200).json({
          success: true,
          msg: "Usuario eliminado"
        });
      } else {
        User.closeConexion();
        res.status(405).json({
          success: false,
          msg: "No se encontró el usuario que se desea eliminar"
        });
      }
    });
  });

  // trae un array de objetos con un contributor los contribuidores
  app.get("/getgroups/:contributor", (req, res) => {
    fk_contributor = req.params.contributor;
    Contributor.getGroups(fk_contributor, (err, data) => {
      res.status(200).json(data);
    });
  });// fin 


  app.get("/getuserswithresponse", (req, res) => {
    console.log("peticion");
    User.openConexion();
    User.getUserDetailsExisteResponse((err, data) => {
      User.closeConexion();
      res.status(200).json(data);
    });
  });

  app.post('/updatenamegroup', (req, res) => {
    const contributorData = {
      nombre: req.body.nombre,
      id: req.body.id,
      fk_contributor: req.body.fk_contributor
    };
    Contributor.updatenamegroup(contributorData, (err, data) => {
      res.status(200).json({
        status: 200,
        msg: "Actualizado exitoso"
      });
    });
  });


  //falta agregar el codigo de eliminar
  // se reemplaso por deleteSurvey
  app.post('/deleteUser', (req, res) => {
    let id = req.body.id;
    User.deleteUserRegistry(id, (err, data) => {
      res.status(200).json({
        status: 200,
        success: true,
        msg: "Usuario eliminado"
      });
    });
  });

  app.post('/addPosition', (req, res) => {
    let position = req.body.position;
    let id = req.body.id;
    User.openConexion();
    User.addUserPosition(position, id, (err, data) => {
      if (err == null) {
        User.closeConexion();
        res.status(200).json({
          status: 200,
          success: true,
          msg: "Cargo agregado"
        });
      } else {
        User.closeConexion();
        res.status(200).json({
          status: 404,
          success: false,
          msg: "Error al agregar posicion",
          err: err
        });
      }// fin del else
    });
  });

  // app.get('/migrate', (req, res) => {
  //   User.getAllUser((err, data) => {
  //     // console.log(err)
  //     for (i = 0; i < data.length; i++) {
  //       console.log(data[i].id);
  //       User.getLastVersion(data[i].id, (err, res) => {
  //         User.updateVersionActiva(res.fk_iduser, res.id, (err, res) => {
  //           console.log(res);
  //           if (err == null) {
  //             // console.log("success")
  //             // console.log(res)
  //           }
  //         });
  //       });
  //     }// for 
  //   })
  // });

  // app.get('/migrate', (req, res) => {
  //   let count = 0;
  //   User.getData((err, data) => {
  //     if (err == null) {
  //       for (i = 0; i < data.length; i++) {

  //         User.addData(data[i].id, data[i].email, (err, resData, newId) => {
  //           // console.log(oldData[0].id + ' - ' + newData[0].fk_iduser);

  //           // for (i = 0; i < oldData.length; i++) {
  //           // count++;
  //           // if (oldData.length > 1) {
  //           //   console.log(oldData);
  //           // }

  //           // User.compareData(oldData[i].id, oldData[i].fk_email, newData[i].id, newData[i].fk_iduser, i, (err, res) => {

  //           // });
  //           // }
  //           for (x = 0; x < resData.length; x++) {

  //             User.compareData(resData[x], newId, (err, res) => {

  //             });
  //           }
  //         })
  //       }
  //     } else {
  //       res.status(200).json({
  //         status: 404,
  //         success: false,
  //         msg: "Error al agregar posicion",
  //         err: err
  //       });
  //     }
  //   });
  // });

};